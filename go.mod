module gitlab.com/dbehnke74/updatecheck

require (
	github.com/gopherjs/gopherjs v0.0.0-20180825215210-0210a2f0f73c // indirect
	github.com/jtolds/gls v4.2.1+incompatible // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/kr/pty v1.1.2 // indirect
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/smartystreets/assertions v0.0.0-20180820201707-7c9eb446e3cf // indirect
	github.com/smartystreets/goconvey v0.0.0-20180222194500-ef6db91d284a
	github.com/smartystreets/gunit v0.0.0-20180314194857-6f0d6275bdcd // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.1
)

go 1.13
